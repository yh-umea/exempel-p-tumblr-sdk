//
//  AppDelegate.h
//  TumblrExample
//
//  Created by Johan Forssell on 2015-10-07.
//  Copyright © 2015 Dohi Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

