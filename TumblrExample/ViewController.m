//
//  ViewController.m
//  TumblrExample
//
//  Created by Johan Forssell on 2015-10-07.
//  Copyright © 2015 Dohi Agency. All rights reserved.
//

#import "ViewController.h"
#import <TMTumblrSDK/TMAPIClient.h>


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupTumblrSDK];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self runSomeTumblrCode];
}

#pragma mark - Tumblr

- (void)setupTumblrSDK
{
    [TMAPIClient sharedInstance].OAuthConsumerKey = @"YsKeK4ZgpTT5z7WF2fqI6BaVYYx8AFawFmiTomKbIaAMJQgip7";
    [TMAPIClient sharedInstance].OAuthConsumerSecret = @"cUPbaoWG6EwlxzdrQe7SSMXWpYtzd9BLSlvspDju07JeS8QzSB";
}

- (void)runSomeTumblrCode
{    
    [[TMAPIClient sharedInstance] posts:@"butteryplanet" type:@"text" parameters:nil callback:^(id data, NSError *error) {
        NSString *body_html = data[@"posts"][0][@"body"];
        
        [self.webView loadHTMLString:body_html baseURL:nil];
        
        NSLog(@"%@", body_html);
    }];
}
@end
