//
//  main.m
//  TumblrExample
//
//  Created by Johan Forssell on 2015-10-07.
//  Copyright © 2015 Dohi Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
